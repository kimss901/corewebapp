﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreWebApp.JsonConvertor;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace CoreWebApp.Models
{
    public class Book
    {

        [JsonConverter(typeof(ObjectIdConverter))]

        public ObjectId Id { get; set; }

        [BsonElement("name")]
        public string BookName { get; set; }

        [BsonElement("price")]
        public decimal Price { get; set; }

        [BsonElement("category")]
        public string Category { get; set; }

        [BsonElement("author")]
        public string Author { get; set; }
    }
}
