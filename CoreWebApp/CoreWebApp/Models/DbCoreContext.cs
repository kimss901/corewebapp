﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreWebApp.Models;

namespace CoreWebApp.Models
{
    public class DbCoreContext : DbContext
    {
        public DbCoreContext(DbContextOptions<DbCoreContext> options) : base(options)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; }

        public DbSet<CoreWebApp.Models.Book> Books { get; set; }
    }
}
