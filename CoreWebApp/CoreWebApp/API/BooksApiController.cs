﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoreWebApp.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;

namespace CoreWebApp.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksApiController : ControllerBase
    {
        private readonly IMongoCollection<Book> _books;

        public BooksApiController(IConfiguration _config)
        {
            var client = new MongoClient(_config.GetConnectionString("BookStoreDb"));
            var database = client.GetDatabase("BookStoreDb");
            _books = database.GetCollection<Book>("Books");
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Book>>> GetBooks()
        {
            var books = _books.Find<Book>(book => true).ToList();
            return books;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetBookbyId(string id)
        {
            var book = _books.Find<Book>(b => b.Id == ObjectId.Parse(id)).ToList();

            return book[0];
        }

        [HttpPost]
        public ActionResult<TodoItem> PostBook([FromBody]Book book)
        {
            _books.InsertOne(book);
            return CreatedAtAction("GetBookbyId", new { id = book.Id }, book);
        }


        [HttpPut("{id}")]
        public IActionResult PutTodoItem(string id, Book book)
        {
            var docId = new ObjectId(id);


            if (docId != book.Id)
                return BadRequest();


            _books.ReplaceOne(b => b.Id == docId, book);

            
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<TodoItem> DeleteTodoItem(string id)
        {
            var docId = new ObjectId(id);

            _books.DeleteOne(b => b.Id == docId);
            return NoContent();

        }

    }
}