﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CoreWebApp.Models;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Logging;
using CoreWebApp.Utility;

namespace CoreWebApp.Controllers
{
    public class TodoItemsController : Controller
    {
        HttpClient client = new HttpClient();
        private readonly ILogger _logger;
        Models.DbCoreContext _context;
        public TodoItemsController(Models.DbCoreContext context, ILogger<TodoItemsController> logger)
        {
            _context = context;
            _logger = logger;
            client.BaseAddress = new Uri("https://localhost:44364/api/");
            logger.LogInformation("Service Address is ::::::" + client.BaseAddress.ToString());
            logger.LogError("Error Service Address is ::::::" + client.BaseAddress.ToString());

        }

        // GET: TodoItems
        public async Task<IActionResult> Index()
        {
            var response = client.GetAsync("TodoApi");
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var readTask = response.Result.Content.ReadAsAsync<IList<TodoItem>>();
                readTask.Wait();

                return View(readTask.Result);

            }
            else
            {
                ModelState.AddModelError("", "Please contact Administrator.");
                return NoContent();
            }            
        }

        // GET: TodoItems/Details/5
        public IActionResult Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var response = client.GetAsync("TodoApi/" + id.ToString());
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var readTask = response.Result.Content.ReadAsAsync<TodoItem>();
                readTask.Wait();

                return View(readTask.Result);

            }
            else
            {
                return NotFound();
            }
        }

        // GET: TodoItems/Create
        public IActionResult Create()
        {
            throw new DivideByZeroException();
            return View();
        }

        // POST: TodoItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,IsComplete")] TodoItem todoItem)
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("TodoApi", todoItem);
                response.EnsureSuccessStatusCode();

                return RedirectToAction(nameof(Index));

            }
            return View(todoItem);
        }

        // GET: TodoItems/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var todoItem = await _context.TodoItems.FindAsync(id);
            if (todoItem == null)
            {
                return NotFound();
            }
            return View(todoItem);
        }

        // POST: TodoItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Name,IsComplete")] TodoItem todoItem)
        {
            if (id != todoItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                HttpResponseMessage response = await client.PutAsJsonAsync("todoApi/" + id.ToString(), todoItem);
                response.EnsureSuccessStatusCode();

                return RedirectToAction(nameof(Index));
            }
            return View(todoItem);
        }

        // GET: TodoItems/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var todoItem = await _context.TodoItems
                .FirstOrDefaultAsync(m => m.Id == id);
            if (todoItem == null)
            {
                return NotFound();
            }

            return View(todoItem);
        }

        // POST: TodoItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            HttpResponseMessage response = await client.DeleteAsync("todoApi/" + id.ToString());
            response.EnsureSuccessStatusCode();
            return RedirectToAction(nameof(Index));
        }

        
    }
}
