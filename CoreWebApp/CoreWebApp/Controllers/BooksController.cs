﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CoreWebApp.Models;
using MongoDB.Bson;
using System.Net.Http;

namespace CoreWebApp.Controllers
{
    public class BooksController : Controller
    {
        
        HttpClient client = new HttpClient();

        public BooksController()
        {
            
            client.BaseAddress = new Uri("https://localhost:44364/api/");

        }

        // GET: Books
        public async Task<IActionResult> Index()
        {
            

            var response = client.GetAsync("BooksApi");
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var readTask = response.Result.Content.ReadAsAsync<IList<Book>>();
                readTask.Wait();

                return View(readTask.Result);

            }
            else
            {
                ModelState.AddModelError("", "Please contact Administrator.");
                return NoContent();
            }

            //return View(await _context.Books.ToListAsync());
        }

        // GET: Books/Details/5
        public async Task<IActionResult> Details(String id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var response = client.GetAsync("BooksApi/" + id.ToString());
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var readTask = response.Result.Content.ReadAsAsync<Book>();
                readTask.Wait();

                return View(readTask.Result);

            }
            else
            {
                return BadRequest();
            }

        }

        // GET: Books/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,BookName,Price,Category,Author")] Book book)
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage response = await client.PostAsJsonAsync("BooksApi", book);
                response.EnsureSuccessStatusCode();

                return RedirectToAction(nameof(Index));

            }
            
            return View(book);
        }

        // GET: Books/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var response = client.GetAsync("BooksApi/" + id.ToString());
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var readTask = response.Result.Content.ReadAsAsync<Book>();
                readTask.Wait();

                return View(readTask.Result);

            }
            else
            {
                return BadRequest();
            }
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,BookName,Price,Category,Author")] Book book)
        {

           
            if (ModelState.IsValid)
            {
                book.Id = ObjectId.Parse(id);
                HttpResponseMessage response = await client.PutAsJsonAsync("BooksApi/" + id.ToString(), book);
                response.EnsureSuccessStatusCode();

                return RedirectToAction(nameof(Index));
            }
            return View(book);


        }

        // GET: Books/Delete/5
        public async Task<IActionResult> Delete(string  id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var response = client.GetAsync("BooksApi/" + id.ToString());
            response.Wait();

            if (response.Result.IsSuccessStatusCode)
            {
                var readTask = response.Result.Content.ReadAsAsync<Book>();
                readTask.Wait();

                return View(readTask.Result);

            }
            else
            {
                return BadRequest();
            }
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            HttpResponseMessage response = await client.DeleteAsync("BooksApi/" + id.ToString());
            response.EnsureSuccessStatusCode();

            return RedirectToAction(nameof(Index));

        }

    }
}
